package com.atguigu.springcloud.lb;

import org.springframework.cloud.client.ServiceInstance;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.concurrent.atomic.AtomicInteger;

//手写轮询算法实现负载均衡
@Component
public class MyLb implements LoadBalancer{

    private AtomicInteger atomicInteger=new AtomicInteger(0);

    public final int getAndIncrement(){
        int current;
        int next;
        do {
            current=this.atomicInteger.get();//初始值是0
            next=current>=2147483647?0:current+1;
        }while (!this.atomicInteger.compareAndSet(current,next));//期望值是current，修改值是next,如果不OK一直取反自旋，取到想要的值为止
        System.out.println("********第几次访问，次数next: "+next);
        return next;
    }
    @Override
    public ServiceInstance instances(List<ServiceInstance> serviceInstances) {
        int index = getAndIncrement() % serviceInstances.size();//轮询算法计算访问服务器下标

        return serviceInstances.get(index);
    }
}
