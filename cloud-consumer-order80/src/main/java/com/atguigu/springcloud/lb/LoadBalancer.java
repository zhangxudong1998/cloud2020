package com.atguigu.springcloud.lb;

import org.springframework.cloud.client.ServiceInstance;

import java.util.List;


public interface LoadBalancer {
    //1.收集eureka上活着的服务器集群总数
    ServiceInstance instances(List<ServiceInstance> serviceInstances);
}
